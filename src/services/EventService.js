import axios from 'axios'

let baseURL;
if(process.env.NODE_ENV === 'production')
  baseURL = process.env.VUE_APP_JSON_SERVER_URL;
else
  baseURL = 'http://localhost:3000';


const apiClient = axios.create({
  baseURL,
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getEvents(perPage, page) {
    return apiClient.get('/events?_limit=' + perPage + '&_page=' + page)
  },
  getEvent(id) {
    return apiClient.get('/events/' + id)
  },
  postEvent(event) {
    return apiClient.post('/events', event)
  }
}
